function cambio(){
    const moneda1 = document.getElementById('opcion1');
	const moneda2 = document.getElementById('opcion2');
	let contenido = "";
    let contenido2 = "";
	
    if(parseInt(moneda1.value) == 1){
        contenido = `<option value="6">Dólar estadounidense</option>
			<option value="7">Dolar canadiense</option>
			<option value="8">Euro</option>`
        contenido2 = `  <option value="1" selected>Peso Mexicano</option>
                        <option value="2">Dolar estadounidense</option>
                        <option value="3">Dolar canadiense</option>
                        <option value="4">Euro</option>`
    }if(parseInt(moneda1.value) == 2){
        contenido = `<option value="5">Peso Mexicano</option>
			<option value="7">Dolar canadiense</option>
			<option value="8">Euro</option>`
        contenido2 = `  <option value="1">Peso Mexicano</option>
                        <option value="2" selected>Dolar estadounidense</option>
                        <option value="3">Dolar canadiense</option>
                        <option value="4">Euro</option>`
        
    }if(parseInt(moneda1.value) == 3){
        contenido = `<option value="5">Peso Mexicano</option>
			<option value="6">Dolar estadounidense</option>
			<option value="8">Euro</option>`
        contenido2 = `  <option value="1">Peso Mexicano</option>
                        <option value="2">Dolar estadounidense</option>
                        <option value="3" selected>Dolar canadiense</option>
                        <option value="4">Euro</option>`
    }if(parseInt(moneda1.value) == 4){
        contenido = `<option value="5">Peso Mexicano</option>
			<option value="6">Dolar estadounidense</option>
			<option value="7">Dolar canadiense</option>`
        contenido2 = `  <option value="1">Peso Mexicano</option>
                        <option value="2">Dolar estadounidense</option>
                        <option value="3">Dolar canadiense</option>
                        <option value="4" selected>Euro</option>`
    }

	moneda2.innerHTML = contenido;
    moneda1.innerHTML = contenido2;
}

function calcular(){
    let valor = document.getElementById('cantidad').value;
    let moneda1 = document.getElementById('opcion1');
	let moneda2 = document.getElementById('opcion2');
    let subtotal = document.getElementById('subtotal');
    let comision = document.getElementById('comision');
    let total = document.getElementById('total');


    // Pesos
    if(parseInt(moneda1.value) == 1 && parseInt(moneda2.value) == 6){
        subtotal.value = valor / 19.85;
    }else if(parseInt(moneda1.value) == 1 && parseInt(moneda2.value) == 7){
        subtotal.value = valor / 14.70;
    }else if(parseInt(moneda1.value) == 1 && parseInt(moneda2.value) == 8){
        subtotal.value = valor / 20.05;
    }

    // Dolares
    else if(parseInt(moneda1.value) == 2 && parseInt(moneda2.value) == 5){
        subtotal.value = valor * 19.85;
    }else if(parseInt(moneda1.value) == 2 && parseInt(moneda2.value) == 7){
        subtotal.value = valor * 1.35;
    }else if(parseInt(moneda1.value) == 2 && parseInt(moneda2.value) == 8){
        subtotal.value = valor * 0.99;
    }

    // Dolares canadiences
    else if(parseInt(moneda1.value) == 3 && parseInt(moneda2.value) == 5){
        subtotal.value =  (valor / 1.35) * 19.85;
    }else if(parseInt(moneda1.value) == 3 && parseInt(moneda2.value) == 6){
        subtotal.value = (valor / 1.35);
    }else if(parseInt(moneda1.value) == 3 && parseInt(moneda2.value) == 8){
        subtotal.value =  (valor / 1.35) * 0.99;
    }

    // Euro
    else if(parseInt(moneda1.value) == 4 && parseInt(moneda2.value) == 5){
        subtotal.value = (valor / 0.99) * 19.85;
    }else if(parseInt(moneda1.value) == 4 && parseInt(moneda2.value) == 6){
        subtotal.value = (valor / 0.99);
    }else if(parseInt(moneda1.value) == 4 && parseInt(moneda2.value) == 7){
        subtotal.value = (valor / 0.99) * 1.35;
    }
    


    comision.value = (subtotal.value * .03).toFixed(2);
    total.value = (parseFloat(subtotal.value) + parseFloat(comision.value)).toFixed(2) ;  
}

let Fsubtotal = 0;
let Fcomision = 0;
let Ftotal = 0;

function registro(){
    let valor = document.getElementById('cantidad');
    let moneda1 = document.getElementById('opcion1');
	let moneda2 = document.getElementById('opcion2');
    let subtotal = document.getElementById('subtotal');
    let comision = document.getElementById('comision');
    let total = document.getElementById('total');
    let registro = document.getElementById('registro');
    let sumatoria = document.getElementById('sumatoria');

    if(parseInt(moneda1.value) == 1 && parseInt(moneda2.value) == 6){
        registro.innerText += `se cambiaron ${valor.value} Pesos Mexicanos a Dolares Estadounidense con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }else if(parseInt(moneda1.value) == 1 && parseInt(moneda2.value) == 7){
        registro.innerText += `se cambiaron ${valor.value} Pesos Mexicanos a Dolares Canadienses con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }else if(parseInt(moneda1.value) == 1 && parseInt(moneda2.value) == 8){
        registro.innerText += `se cambiaron ${valor.value} Pesos Mexicanos a Euros con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }

    // Dolares
    else if(parseInt(moneda1.value) == 2 && parseInt(moneda2.value) == 5){
        registro.innerText += `se cambiaron ${valor.value} Dolares Estadounidense a Pesos Mexicanos con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }else if(parseInt(moneda1.value) == 2 && parseInt(moneda2.value) == 7){
        registro.innerText += `se cambiaron ${valor.value} Dolares Estadounidense a Dolares Canadienses con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }else if(parseInt(moneda1.value) == 2 && parseInt(moneda2.value) == 8){
        registro.innerText += `se cambiaron ${valor.value} Dolares Estadounidense a Euros con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }

    // Dolares canadiences
    else if(parseInt(moneda1.value) == 3 && parseInt(moneda2.value) == 5){
        registro.innerText += `se cambiaron ${valor.value} Dolares Canadienses a Pesos Mexicanos con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }else if(parseInt(moneda1.value) == 3 && parseInt(moneda2.value) == 6){
        registro.innerText += `se cambiaron ${valor.value} Dolares Canadienses a Dolares Estadounidense con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }else if(parseInt(moneda1.value) == 3 && parseInt(moneda2.value) == 8){
        registro.innerText += `se cambiaron ${valor.value} Dolares Canadienses a Euros con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }

    // Euro
    else if(parseInt(moneda1.value) == 4 && parseInt(moneda2.value) == 5){
        registro.innerText += `se cambiaron ${valor.value} Euros a Pesos Mexicanos con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }else if(parseInt(moneda1.value) == 4 && parseInt(moneda2.value) == 6){
        registro.innerText += `se cambiaron ${valor.value} Euros a Dolares Estadounidense con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }else if(parseInt(moneda1.value) == 4 && parseInt(moneda2.value) == 7){
        registro.innerText += `se cambiaron ${valor.value} Euros a Dolares Canadienses con un total de: ${subtotal.value} con una comision: ${comision.value} y el total a pagar es: ${total.value} \n`;
    }
   

    Fsubtotal += parseFloat(subtotal.value);
    Fcomision += parseFloat(comision.value);
    Ftotal += parseFloat(total.value);


    sumatoria.innerText = `Sub total: ${parseFloat(Fsubtotal)} Comision: ${parseFloat(Fcomision)} Total: ${parseFloat(Ftotal)}`;
}

function limpiar(){
    let registros = document.getElementById('registro');
    let totales = document.getElementById('sumatoria');
    let subtotal = document.getElementById('subtotal');
    let comision = document.getElementById('comision');
    let total = document.getElementById('total');
    subtotal.value = "";
    comision.value = "";
    total.value = "";
    registros.innerHTML = "";
    totales.innerHTML = "";
    Fsubtotal = 0;
    Ftotal = 0;
    Fcomision = 0;
}